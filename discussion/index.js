console.log("Hello from JS");

/* multi-line comment
console.log("hello fromm JS");
console.log("hello fromm JS");
console.log("hello fromm JS");
console.log("hello fromm JS");
console.log("hello fromm JS");
*/

// console.log("end of comment");

console.log("example statement");

let clientName =  "John D LaCruz"
let contactNumber = "09191234567"

let greetings;


console.log(clientName);
console.log(contactNumber);
console.log(greetings);
let pangalan = "John Doe"
console.log(pangalan);


// 1. String
let country = "Philippines";
let province = 'Metro Manila';
// 2. Number
let headcount = 26;
let grade = 98.7
let planetDistance = 2e10;
// 3. Boolean
let isMarried = false;
let inGoodConduct = true;
// 4. Null
let spouse = null;
console.log(spouse)
let criminalRecords = true;
// 5. Undefined
let fullName;
// 6. Arrays
let grades = [98.7, 92.1, 90.2, 94.6]
// 7.Objeccts

// Array - special kind of composite data type that is used to store multiple values
let bootcampSubject = ["HTML","CSS","Bootstrap","Javascript"];
console.log(bootcampSubject)


let details = ["Keanu","Reeves",32,true];
console.log(details);

let cellphone = {
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'AX12002122',
	isHomeCredit: true,
	features: ["Calling", "Texting", "Ringing", "5G"],
	price: 8000
}

console.log(cellphone);


let personName = "Mich";
console.log(personName);

personName =  'Memichie'
console.log(personName);



// concatenate string (+)
	// join, combine, link
let pet = "dog";
console.log("this is the initial value of var: " + pet);

pet = "cat";
console.log("this is the new value of the var: " + pet);


// constants
	// permanent, fixed, absolute
// syntax: const desiredName = value;
const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(famliyName);

// familyName =  'Castillo';
// console.log(famliyName);